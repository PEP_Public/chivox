
Pod::Spec.new do |s|

  s.name         = "chivox"
  s.version      = "0.0.5"
  s.summary      = "驰声SDK"
  s.author       = { "chivox" => "PEP" }
  s.platform     =  :ios, "7.0"

  s.homepage     = "https://gitlab.com/PEP_Public/chivox"
  s.source       = { :git => "https://gitlab.com/PEP_Public/chivox.git" }

  s.subspec "chivox" do |chivox|
#    chivox.subspec "core" do |core|
#      core.source_files = "chivox/*.{c,h,m}"
#    end

    chivox.subspec "audio" do |audio|
      audio.source_files = "chivox/audio/*.{c,h,m}"
    end

    chivox.vendored_libraries = 'chivox/libaiengine_iphoneos.a', 'chivox/libcrypto.a', 'chivox/libssl.a'
  end

  s.frameworks   =  "AudioToolbox", "CoreFoundation", "CoreAudio", "SystemConfiguration"
  s.requires_arc = true

end
